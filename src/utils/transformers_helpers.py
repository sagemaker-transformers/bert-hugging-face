import numpy as np
import torch
from transformers import DistilBertForSequenceClassification, DistilBertTokenizer, Trainer
from scipy.special import softmax


class CustomDataset(torch.utils.data.Dataset):
    """Inherits from torch.utils.data.Dataset.
    Arguments:
        encodings (transformers.tokenization_utils_base.BatchEncoding): Tokenized text encodings (with attention masks).
        labels (list): List of the labals corresponding to each text classification
    """
    def __init__(self, encodings, labels=None):
        self.encodings = encodings
        self.labels = labels

    def __getitem__(self, idx):
        """Returns the model inputs at the specified index."""
        item = {key: torch.tensor(val[idx]) for key, val in self.encodings.items()}
        if self.labels:
            item["labels"] = torch.tensor(self.labels[idx])
        return item

    def __len__(self):
        """Returns the length of the dataset."""
        return len(self.encodings["input_ids"])


class TransformersInferenceBERT:
    """Describe..."""

    def __init__(self, model_path):
        """Describe..."""
        self.model_path = model_path
        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        self.model = DistilBertForSequenceClassification.from_pretrained(model_path).to(self.device)
        self.tokenizer = DistilBertTokenizer.from_pretrained(model_path)
        self.trainer = Trainer(model=self.model)

    def single_text_predict(self, text):
        """Describe..."""
        tokens = self.tokenizer([text])
        inference_dataset = CustomDataset(tokens)

        raw_output = self.trainer.predict(inference_dataset)
        probability_output = softmax(raw_output.predictions, axis=1)

        # Model Results compilation
        # Classification
        probabilities = {}
        for i, key in enumerate(self.model.config.label2id.keys()):
            probabilities[key] = np.float64(probability_output[0][i])

        classification = [i for i in probabilities.keys() if probabilities[i] == max(probabilities.values())][0]

        return probabilities, classification
