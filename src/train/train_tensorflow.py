import pandas as pd
import argparse
import os
import json
from transformers import DistilBertTokenizer
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, precision_recall_fscore_support
from transformers import TFDistilBertForSequenceClassification, TFTrainer, TFTrainingArguments, AutoConfig
# import awswrangler as wr
# import sagemaker
import tensorflow as tf
import boto3


def model_fn(model_dir):
    """Load the PyTorch model from the `model_dir` directory."""
    print("Loading model.")

    # Determine model path
    model_path = os.path.join(model_dir, 'model.pth')

    # Load model
    model = TFDistilBertForSequenceClassification.from_pretrained(model_path)

    print("Done loading model.")
    return model


def compute_metrics(pred):
    labels = pred.label_ids
    preds = pred.predictions.argmax(-1)
    precision, recall, f1, _ = precision_recall_fscore_support(labels, preds, average='weighted')
    acc = accuracy_score(labels, preds)
    return {
        'accuracy': acc,
        'f1': f1,
        'precision': precision,
        'recall': recall
    }


if __name__ == '__main__':
    # All of the model parameters and training parameters are sent as arguments when the script
    # is executed. Here we set up an argument parser to easily access the parameters.
    parser = argparse.ArgumentParser()

    # hyperparameters sent by the client are passed as command-line arguments to the script.
    parser.add_argument("--epochs", type=int, default=3)
    parser.add_argument("--train-batch-size", type=int, default=32)
    parser.add_argument("--eval-batch-size", type=int, default=64)
    parser.add_argument("--warmup_steps", type=int, default=500)
    parser.add_argument("--learning_rate", type=float, default=5e-5)
    parser.add_argument('--adam_epsilon', type=float, default=1e-8)

    # SageMaker Parameters
    parser.add_argument('--hosts', type=list, default=json.loads(os.environ['SM_HOSTS']))
    parser.add_argument('--current-host', type=str, default=os.environ['SM_CURRENT_HOST'])
    # parser.add_argument('--data-dir', type=str, default=os.environ['SM_CHANNEL_TRAINING'])
    parser.add_argument('--num-gpus', type=int, default=os.environ['SM_NUM_GPUS'])

    # Data, model, and output directories
    parser.add_argument("--model_name", type=str)
    parser.add_argument('--model-dir', type=str, default=os.environ['SM_MODEL_DIR'])
    parser.add_argument('--output-dir', type=str, default=os.environ['SM_OUTPUT_DATA_DIR'])
    parser.add_argument("--checkpoints", type=str, default="/opt/ml/checkpoints/")
    parser.add_argument("--n_gpus", type=str, default=os.environ["SM_NUM_GPUS"])
    parser.add_argument("--training_dir", type=str, default=os.environ["SM_CHANNEL_TRAIN"])
    parser.add_argument("--bucket", type=str)
    parser.add_argument("--train_filename", type=str)
    parser.add_argument("--data_labels_dict", type=str)
    parser.add_argument("--text_column", type=str)
    parser.add_argument("--encoded_label_column", type=str)
    parser.add_argument("--test_split_size", type=float, default=0.1)

    args = parser.parse_args()

    df = pd.read_parquet(os.path.join(args.training_dir, args.train_filename))

#     # Set up logging
#     logger = logging.getLogger(__name__)

#     logging.basicConfig(
#         level=logging.getLevelName("INFO"),
#         handlers=[logging.StreamHandler(sys.stdout)],
#         format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
#     )

    texts = df[args.text_column].values
    labels = df[args.encoded_label_column].values

    texts = list(texts)
    labels = list(labels)

    train_texts, val_texts, train_labels, val_labels = train_test_split(texts,
                                                                        labels,
                                                                        random_state=111,
                                                                        test_size=args.test_split_size)
    pretrained_model = args.model_name

    tokenizer = DistilBertTokenizer.from_pretrained(pretrained_model, do_lower_case=False)

    train_encodings = tokenizer(train_texts, truncation=True, padding='max_length', return_token_type_ids=True)
    val_encodings = tokenizer(val_texts, truncation=True, padding='max_length', return_token_type_ids=True)

    train_dataset = tf.data.Dataset.from_tensor_slices((
        {"input_ids": train_encodings.input_ids, "attention_mask": train_encodings.attention_mask, "token_type_ids": train_encodings.token_type_ids},
        train_labels
    ))

    val_dataset = tf.data.Dataset.from_tensor_slices((
        {"input_ids": val_encodings.input_ids, "attention_mask": val_encodings.attention_mask, "token_type_ids": val_encodings.token_type_ids},
        val_labels
    ))

    # Load the Labels Dictionary from an S3 JSON file
    s3 = boto3.client('s3')
    response = s3.get_object(Bucket=args.bucket, Key=args.data_labels_dict)
    response_loaded = json.loads(response['Body'].read().decode('utf-8'))

    # Define label mappings as dictionaries
    id2label = {int(key): val for key, val in response_loaded.items()}
    label2id = {val: key for key, val in id2label.items()}

    # Define Configurations of the Huggingface Model
    config = AutoConfig.from_pretrained(pretrained_model, label2id=label2id, id2label=id2label)
    
    # In the case of Huggingface TensorFlow, tou need to specify the training arguments before loading the model
    logs_out_dir = f'{args.output_dir}/logs'

    training_args = TFTrainingArguments(
        # output_dir=artifacts_out_dir,
        output_dir=args.checkpoints,
        num_train_epochs=args.epochs,
        per_device_train_batch_size=args.train_batch_size,
        per_device_eval_batch_size=args.eval_batch_size,
        evaluation_strategy="steps",
        warmup_steps=args.warmup_steps,
        weight_decay=0.01,
        # logging_dir=artifacts_out_dir + './logs',
        logging_dir=logs_out_dir,
        logging_steps=10,
        adam_epsilon=args.adam_epsilon,
        learning_rate=args.learning_rate
    )

    # Define Model from configuration
    with training_args.strategy.scope():
        model = TFDistilBertForSequenceClassification.from_pretrained(pretrained_model, config=config)

    trainer = TFTrainer(
        model=model,
        args=training_args,
        compute_metrics=compute_metrics,
        train_dataset=train_dataset,
        eval_dataset=val_dataset
    )

    trainer.train()

    # evaluate model
    eval_result = trainer.evaluate(eval_dataset=val_dataset)

    # writes eval result to file which can be accessed later in s3 ouput
    with open(os.path.join(args.checkpoints, f"{args.output_dir}/eval_results.txt"), "w") as writer:
        print(f"***** Eval results *****")
        for key, value in sorted(eval_result.items()):
            writer.write(f"{key} = {value}\n")

    # Save the model parameters
    model_path = os.path.join(args.model_dir, 'model.pth')

    trainer.save_model(model_path)
    trainer.model.save_pretrained(model_path, saved_model=True)
    tokenizer.save_pretrained(model_path)
