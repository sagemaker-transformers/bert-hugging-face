import os
import numpy as np
import torch.utils.data
from transformers import DistilBertTokenizer
import torch
from transformers import DistilBertForSequenceClassification, Trainer
from scipy.special import softmax
import json
# import awswrangler as wr
# import sagemaker
# import mlflow

JSON_CONTENT_TYPE = 'application/json'


class InferenceDataset(torch.utils.data.Dataset):
    """Inherits from torch.utils.data.Dataset.
    Arguments:
        encodings (transformers.tokenization_utils_base.BatchEncoding): Tokenized text encodings (with attention masks).
        labels (list): List of the labals corresponding to each text classification
    """
    def __init__(self, encodings, labels=None):
        self.encodings = encodings
        self.labels = labels

    def __getitem__(self, idx):
        """Returns the model inputs at the specified index."""
        item = {key: torch.tensor(val[idx]) for key, val in self.encodings.items()}
        if self.labels:
            item["labels"] = torch.tensor(self.labels[idx])
        return item

    def __len__(self):
        """Returns the length of the dataset."""
        return len(self.encodings["input_ids"])


def model_fn(model_dir):
    """Load the PyTorch model from the `model_dir` directory."""
    print("Loading model.")

    # Determine the device and construct the model.
    model_path = os.path.join(model_dir, 'model.pth')
    print(f"Model path: {model_path}")
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model = DistilBertForSequenceClassification.from_pretrained(model_path)
    model.to(device)

    print("Done loading model.")
    return model


def input_fn(serialized_input_data, content_type=JSON_CONTENT_TYPE):
    """Describe..."""
    print('Deserializing the input data.')
    
    if content_type == JSON_CONTENT_TYPE:
        input_data = json.loads(serialized_input_data)
        return input_data
    else:
        raise Exception(f'Unsupported input type: {content_type}')


def output_fn(prediction_output, accept=JSON_CONTENT_TYPE):
    """Describe..."""
    print('Serializing the generated output.')
    if accept == JSON_CONTENT_TYPE:
        return json.dumps(prediction_output), accept
    else:
        raise Exception(f'Unsupported output type: {accept}')


def predict_fn(input_data, model):
    print('Inferring sentiment of input data.')
    
    print(f"Data Input: {input_data}")
    
    text = input_data["text"]

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model.to(device)

    # the instantiated Huggingface Transformers model to be trained
    trainer = Trainer(model=model)

    # Create Tokenized inputs and dataset
    tokenizer = DistilBertTokenizer.from_pretrained(model.name_or_path)
    inference_encodings = tokenizer([text], truncation=True, padding="max_length")
    inference_dataset = InferenceDataset(inference_encodings)

    # Inference
    output = trainer.predict(inference_dataset)
    output_probabilities = softmax(output.predictions, axis=1)[0]

    # Compile final results
    result = {}
    for i, key in enumerate(model.config.label2id.keys()):
        result[key] = np.float64(output_probabilities[i])

    # Alternative with transformers pipelines
    # nlp = pipeline('text-classification', model=trainer.model, tokenizer=tokenizer, return_all_scores=True)
    # nlp_output = nlp(text)
    # result = {i['label']: i['score'] for i in nlp_output[0]}

    return result
