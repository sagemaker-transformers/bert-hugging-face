import os
from transformers import DistilBertTokenizer
import tensorflow as tf
from transformers import TFDistilBertForSequenceClassification, TFTrainer, pipeline
import json
# import awswrangler as wr
# import sagemaker
# import mlflow

JSON_CONTENT_TYPE = 'application/json'


def model_fn(model_dir):
    """Load the PyTorch model from the `model_dir` directory."""
    print("Loading model.")
    print(model_dir)

    # Determine the device and construct the model.
    model_path = os.path.join(model_dir, 'model.pth')
    print(f"Model path: {model_path}")

    model = TFDistilBertForSequenceClassification.from_pretrained(model_path)
    print("Done loading model.")
    return model


def input_fn(serialized_input_data, content_type=JSON_CONTENT_TYPE):
    """Describe..."""
    print('Deserializing the input data.')
    
    if content_type == JSON_CONTENT_TYPE:
        input_data = json.loads(serialized_input_data)
        return input_data
    else:
        raise Exception(f'Unsupported input type: {content_type}')


def output_fn(prediction_output, accept=JSON_CONTENT_TYPE):
    """Describe..."""
    print('Serializing the generated output.')
    if accept == JSON_CONTENT_TYPE:
        return json.dumps(prediction_output), accept
    else:
        raise Exception(f'Unsupported output type: {accept}')


def predict_fn(input_data, model):
    print('Inferring sentiment of input data.')
    
    print(f"Data Input: {input_data}")
    
    text = input_data["text"]

    # the instantiated Huggingface Transformers model to be trained
#     trainer = TFTrainer(model=model)

    # Create Tokenized inputs and dataset
    tokenizer = DistilBertTokenizer.from_pretrained(model.name_or_path)
    inference_inputs = tokenizer.encode_plus(text, return_tensors="tf")

    # Inference
#     output = trainer.model(inference_inputs)
    output = model(inference_inputs)
    output_probabilities = tf.nn.softmax(output.logits).numpy()

    # Compile final results
    result = {}
    for i, key in enumerate(model.config.label2id.keys()):
        result[key] = output_probabilities[0][i]

    # Alternative with transformers pipelines
    # nlp = pipeline('text-classification', model=trainer.model, tokenizer=tokenizer, return_all_scores=True)
    # nlp_output = nlp(text)
    # result = {i['label']: i['score'] for i in nlp_output[0]}

    return result
