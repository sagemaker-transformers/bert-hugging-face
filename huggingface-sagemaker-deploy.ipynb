{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Deploying a Huggingface model to an Endpoint\n",
    "## Using PyTorch and SageMaker\n",
    "\n",
    "---\n",
    "\n",
    "### General Outline\n",
    "\n",
    "The code in this notebook will serve as an example to deploy a previously trained Huggingface model through SageMaker.\n",
    "\n",
    "You can see some reference to this code in the [Sagemaker Fridays - Import / Export Models episode](https://www.youtube.com/watch?v=ZgV894sQxSU) (where there is a segment about BERT model deployment).\n",
    "\n",
    "At the moment, SageMaker does not have a specific Inference Framework for Huggingface models, as it has for its Model Training Estimators. In any case, these models are using one of the 2 frameworks:\n",
    "- PyTorch\n",
    "- Tensorflow\n",
    "\n",
    "So, you only need to make your deployment according to which framework you decided to train a model."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# !pip install --upgrade sagemaker"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import sagemaker\n",
    "\n",
    "sagemaker_session = sagemaker.Session(default_bucket=\"<your-s3bucket-here>\")\n",
    "bucket = sagemaker_session.default_bucket()\n",
    "region = sagemaker_session._region_name\n",
    "role = sagemaker.get_execution_role()\n",
    "\n",
    "print(f\"{sagemaker_session}\\n{bucket}\\n{region}\\n{role}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "## Model Serve Script\n",
    "\n",
    "We'll need to use the entry script and directory, in a similar way as we provided when creating the model. However, since we now wish to accept a string as input and our model expects a processed review, we need to write some custom inference code.\n",
    "\n",
    "We will store the code that we write in the `serve` directory. Our `predict_pytorch.py` is the file which will contain custom inference code (using the PyTorch Framework). \n",
    "Note also that `requirements.txt` is present which will tell SageMaker what Python libraries are required by our custom inference code.\n",
    "\n",
    "When deploying a PyTorch model in SageMaker, you are expected to provide four functions which the SageMaker inference container will use.\n",
    " - `model_fn`: This function is the same function that we used in the training script and it tells SageMaker how to load our model.\n",
    " - `input_fn`: This function receives the raw serialized input that has been sent to the model's endpoint and its job is to de-serialize and make the input available for the inference code.\n",
    " - `output_fn`: This function takes the output of the inference code and its job is to serialize this output and return it to the caller of the model's endpoint.\n",
    " - `predict_fn`: The heart of the inference script, this is where the actual prediction is done and is the function which you will need to complete.\n",
    "\n",
    "For the simple website that we are constructing during this project, the `input_fn` and `output_fn` methods are relatively straightforward. We only require being able to accept a string as input and we expect to return a single value as output. You might imagine though that in a more complex application the input or output may be image data or some other binary data which would require some effort to serialize."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!pygmentize src/serve/predict_pytorch.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "## Deploying the model\n",
    "\n",
    "Now that the custom inference code has been written, we will create and deploy our model. To begin with, we need to construct a new PyTorchModel object which points to the model artifacts created during training and also points to the inference code that we wish to use. Then we can call the deploy method to launch the deployment container.\n",
    "\n",
    "**NOTE**: The default behaviour for a deployed PyTorch model is to assume that any input passed to the predictor is a `numpy` array. In our case we want to send a string so we need to construct a simple wrapper around the `RealTimePredictor` class to accomodate simple strings. In a more complicated situation you may want to provide a serialization object, for example if you wanted to sent image data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sagemaker.estimator import Estimator\n",
    "\n",
    "# job which is going to be attached to the estimator\n",
    "training_job_name='<Your-TrainingJob-Name-Here>'\n",
    "\n",
    "huggingface_estimator = Estimator.attach(training_job_name)\n",
    "\n",
    "print(huggingface_estimator)\n",
    "print(f\"\\nModel Artifacts Path:\\n{huggingface_estimator.model_data}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### (Optional)\n",
    "You can download the artifacts of the trained model to a local path."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sagemaker.s3 import S3Downloader\n",
    "\n",
    "S3Downloader.download(\n",
    "    s3_uri=huggingface_estimator.model_data,   # s3 uri where the trained model is located\n",
    "    local_path='.',                            # local path where *.targ.gz is saved\n",
    "    sagemaker_session=sagemaker_session        # sagemaker session used for training the model\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Create a Predictor Endpoint \n",
    "Using the `src/serve/predict_pytorch.py` script.\n",
    "\n",
    "On the other hand, it is possible to load models directly from [Huggingface Hub](https://huggingface.co/models). In which case you can use the commented code and replace the previously trained estimator model."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sagemaker.huggingface import HuggingFaceModel\n",
    "import sagemaker\n",
    "\n",
    "# Hub Model configuration. https://huggingface.co/models\n",
    "# hub = {\n",
    "#   'HF_MODEL_ID':'distilbert-base-uncased-distilled-squad', # model_id from hf.co/models\n",
    "#   'HF_TASK':'question-answering' # NLP task you want to use for predictions\n",
    "# }\n",
    "\n",
    "# create Hugging Face Model Class\n",
    "huggingface_model = HuggingFaceModel(\n",
    "    model_data=huggingface_estimator.model_data,\n",
    "    #     env=hub,\n",
    "    source_dir='./src/serve',\n",
    "    entry_point='predict_pytorch.py',\n",
    "    role=role, # iam role with permissions to create an Endpoint\n",
    "    transformers_version=\"4.6\", # transformers version used\n",
    "    pytorch_version=\"1.7\", # pytorch version used\n",
    "    py_version=\"py36\", # python version of the DLC\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# deploy model to SageMaker Inference\n",
    "predictor = huggingface_model.deploy(\n",
    "   initial_instance_count=1,\n",
    "   instance_type=\"ml.m5.xlarge\"\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sagemaker.serializers import JSONSerializer\n",
    "from sagemaker.deserializers import JSONDeserializer\n",
    "\n",
    "predictor.serializer = JSONSerializer()\n",
    "predictor.deserializer = JSONDeserializer()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Check out the Endpoint on SageMaker console"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from IPython.core.display import display, HTML\n",
    "\n",
    "display(HTML('<b>Review <a target=\"blank\" href=\"https://console.aws.amazon.com/sagemaker/home?region={}#/endpoints/{}\">SageMaker REST Endpoint</a></b>'.format(\n",
    "    region, predictor.endpoint_name)\n",
    "            ))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Perform inference"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import json"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "test_data = {'text': 'I love this product!'}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "prediction = predictor.predict(test_data)\n",
    "print(type(prediction))\n",
    "print(prediction)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "test_review = 'The simplest pleasures in life are the best, and this film is one of them. Combining a rather basic storyline of love and adventure this movie transcends the usual weekend fair with wit and unmitigated charm.'\n",
    "\n",
    "test_data = {'text': test_review}\n",
    "prediction = predictor.predict(test_data)\n",
    "print(prediction)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**(Optional)** \n",
    "\n",
    "You can create a model class from an image URI. It can be used when you have a custom docker image, if needed. \n",
    "\n",
    "In this case, the code is still not tested."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# inference_image_uri = sagemaker.image_uris.retrieve(\n",
    "#     framework=\"pytorch\",\n",
    "#     region=region,\n",
    "#     version=\"1.6.0\",\n",
    "#     py_version=\"py36\",\n",
    "#     instance_type=\"ml.m5.large\",\n",
    "#     image_scope=\"inference\"\n",
    "# )\n",
    "# print(inference_image_uri)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# from sagemaker.model import Model\n",
    "\n",
    "# model_name = 'bert-model-{}'.format(timestamp)\n",
    "\n",
    "# model = Model(\n",
    "#     name=model_name,\n",
    "#     ### BEGIN SOLUTION - DO NOT delete this comment for grading purposes\n",
    "#     image_uri=inference_image_uri, # Replace None\n",
    "#     ### END SOLUTION - DO NOT delete this comment for grading purposes\n",
    "#     model_data=huggingface_estimator.model_data,\n",
    "#     sagemaker_session=sagemaker_session,\n",
    "#     role=role,\n",
    "# )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If the Endpoint has already been created, use this code to reference it by name."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# from sagemaker.predictor import Predictor\n",
    "# from sagemaker.serializers import JSONSerializer\n",
    "# from sagemaker.deserializers import JSONDeserializer\n",
    "\n",
    "# predictor = Predictor(\n",
    "#     endpoint_name='pytorch-inference-2021-06-27-23-20-36-662',\n",
    "# )\n",
    "\n",
    "# predictor.serializer = JSONSerializer()\n",
    "# predictor.deserializer = JSONDeserializer()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Testing the Model Endpoint\n",
    "\n",
    "Now that we have deployed our model with the custom inference code, we should test to see if everything is working. \n",
    "\n",
    "It is generally a good idea to send several examples of your data to the endpoint. The point is to check if there is any data inconsistency that might jeopardize the inference process.\n",
    "\n",
    "We can also use a `Bulk Test` to get all the predictions from the test set and derive our metrics."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Bulk Test"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "test_set = pd.read_csv(\"data/Corona_NLP_test.csv\", encoding=\"ISO-8859-1\")[:20]\n",
    "\n",
    "test_set.tail()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "test_set['prediction'] = [predictor.predict(tweet) for tweet in test_set['OriginalTweet']]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.metrics import accuracy_score\n",
    "accuracy_score(ground, results)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Delete the endpoint\n",
    "\n",
    "Remember to always shut down your endpoint if you are no longer using it. You are charged for the length of time that the endpoint is running so if you forget and leave it on you could end up with an unexpectedly large bill."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "predictor.endpoint_name"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "predictor.delete_endpoint()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "instance_type": "ml.t3.medium",
  "kernelspec": {
   "display_name": "Python 3 (Data Science)",
   "language": "python",
   "name": "python3__SAGEMAKER_INTERNAL__arn:aws:sagemaker:eu-west-1:470317259841:image/datascience-1.0"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
