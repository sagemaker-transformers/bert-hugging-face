# Colab Notebooks

These Notebooks are a _**Proof of Concept**_. 
That is, you can use these notebooks to observe and test the code used in this repository scripts.

In this way, you can consider them as a `Playground` for whatever different experiments to try on code:
- Trying to mangle with different configuration parameters;
- Try other error metrics during training;
- Observe different ways to provide model outputs (_Possibly classifying in bulk_);
- etc...

Both options of using TensorFlow and PyTorch are provided. 
The notebooks, as well as the overall repository, are focused on a `Text Classification Task`.
For other model Possibilities and text tasks, try the [Huggingface Hub 🤗](https://huggingface.co/models)

Logically, different models will have different ways to handle both inputs and outputs.
This will mean you'll have to test code.
