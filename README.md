# BERT Hugging Face - Using Sagemaker

This repository intends to provide a tutorial for BERT model training, using the new Hugging Face Estimator from Sagemaker.

You will be able to observe the training of a BERT model for text classification, with the Hugging Face (Transformers) library.

This repository also took some references from a series of tutorials from AWS (Sagemaker Fridays). You can check out these links for more information:
- [🤗 Huggingface Transformers page for SageMaker Estimator](https://huggingface.co/transformers/sagemaker.html#create-an-huggingface-estimator)
- [🤗 Huggingface Transformers page for the Trainer / TFTrainer class](https://huggingface.co/transformers/main_classes/trainer.html#transformers.TFTrainer) 
- [Sagemaker Fridays - Hugging Face Estimator episode](https://www.youtube.com/watch?v=vcu4lqcfiR0)
- [Sagemaker Fridays - Hugging Face Estimator Gitlab Code Repository](https://gitlab.com/juliensimon/amazon-studio-demos/-/tree/master/sagemaker_fridays/s03e05)
- [Sagemaker Fridays - Import / Export Models (segment about BERT model deployment)](https://www.youtube.com/watch?v=ZgV894sQxSU)
- [Sagemaker Higgingface Inference](https://aws.amazon.com/blogs/machine-learning/announcing-managed-inference-for-hugging-face-models-in-amazon-sagemaker/)

Naturally, the code showed here goes further beyond the tutorial, actually getting into the side of model deployment. Some of the elements present in the notebooks will be optional, such as setting up aws monitoring and profiler.

The purpose is to provide a full code framework for:
- Training
- Hyperparameter Tuning
- Model Performance Tracking:
    - Sagemaker Metrics capture
    - Tensorboard
    - ML Flow
- Deployment

### Repo Structure

```
📦 bert-hugging-face
 ┣ 📂 colab_notebooks
 ┃ ┣ 📜 IMDb Classification with Trainer - PyTorch.ipynb
 ┃ ┣ 📜 IMDb Classification with Trainer - TensorFlow.ipynb
 ┃ ┗ 📜 README.md
 ┣ 📂 data
 ┃ ┣ 📜 Corona_NLP_test.csv
 ┃ ┗ 📜 Corona_NLP_train.csv
 ┣ 📂 src
 ┃ ┣ 📂 serve
 ┃ ┃ ┣ 📜 predict_pytorch.py
 ┃ ┃ ┣ 📜 predict_tensorflow.py
 ┃ ┃ ┗ 📜 requirements.txt
 ┃ ┣ 📂 train
 ┃ ┃ ┣ 📜 requirements.txt
 ┃ ┃ ┣ 📜 train_pytorch.py
 ┃ ┃ ┗ 📜 train_tensorflow.py
 ┃ ┗ 📂 utils
 ┃ ┃ ┗ 📜 transformers_helpers.py
 ┣ 📜 .gitignore
 ┣ 📜 README.md
 ┣ 📜 huggingface-PyTorch-sagemaker-deploy.ipynb
 ┣ 📜 huggingface-PyTorch-sagemaker-train.ipynb
 ┣ 📜 huggingface-TensorFlow-sagemaker-deploy.ipynb
 ┗ 📜 huggingface-TensorFlow-sagemaker-train.ipynb
```

**Notes**:
- `Colab Notebooks`: For code testing.
- `Notebooks (base directory)`: How to call Model Training and Create Endopoints on SageMaker.
- `source code`
  - `train`: scripts for Model Training.
  - `serve`: scripts for Model Serving - Create and Endpoint.
- `data`: contains csv files for the **Coronavirus NLP dataset**.  
